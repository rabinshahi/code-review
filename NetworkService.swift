//
//  NetworkService.swift
//
//
//  Created by Rabin Shahi on 30/06/2022.
//

import Foundation

/*
 Network layer Wrapper
 Base on Protocol Oriented Programming
 
 Provide the top layer to perform network request such as GET, POST, PUT, DELETE etc..
 */

/** Required properties for network Operation */
protocol Router {
    var url:  NSURL { get }
    var method: String { get }
    var headers: Dictionary<String, String> { get }
    var parameters: Dictionary<String, Any> { get }
}

/** Set the Default Value */
extension Router {
    var method : String { return "GET" }
    var headers : Dictionary<String, String> { return Dictionary()}
    var parameters : Dictionary<String, Any> { return Dictionary()}
}

/** Required Method for networkOperation */
protocol ConstructableRequest: Router {
    func buildRequest() -> NSURLRequest?
}

/** Implementation of defaultMethod */
extension ConstructableRequest {
    func buildRequest() -> NSURLRequest? {
        let request =  NSMutableURLRequest(url: url as URL)
        if method != "GET" {
            request.httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: [])
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        }
        // TODO: Configure for other Methods (DELETE, PUT etc....)
        request.allHTTPHeaderFields = headers
        request.httpMethod = method
        return request
    }
}

/** Create a Requestable protocol */
protocol Requestable: ConstructableRequest {}

/** Implementation of Requestable */
extension Requestable {
    
    func send(success: @escaping (_ result: AnyObject?, _ statusCode:Int?) -> (), failure: @escaping (_ error: NSError) -> ()) {
        let sessionConfig = URLSessionConfiguration.default
        sessionConfig.timeoutIntervalForResource = 120.0
        
        let session = URLSession.shared
        guard let request = buildRequest() else { return }
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { (taskData, taskResponse, taskError) -> Void in
            if let taskError = taskError {
                DispatchQueue.main.async {
                    failure(taskError as NSError)
                }
            } else if let taskData = taskData {
                do {
                    let json = try JSONSerialization.jsonObject(with: taskData, options: [])
                    DispatchQueue.main.async {
                        success(json as AnyObject?, (taskResponse as? HTTPURLResponse)?.statusCode)
                    }
                } catch {
                    DispatchQueue.main.async {
                        success(nil, (taskResponse as? HTTPURLResponse)?.statusCode)
                    }
                }
            } else {
                DispatchQueue.main.async {
                    success(nil, (taskResponse as? HTTPURLResponse)?.statusCode)
                }
            }
        })
        task.resume()
    }
}

/** Create custom router */
enum TestRouter {
    
    static let baseUrl = "https://jsonplaceholder.typicode.com"
    
    case getTodos
    
    // Config Urls
    var url: NSURL {
        switch self {
        case .getTodos:
            return NSURL(string: TestRouter.baseUrl + "/todos/1")!
        }
    }
    
    // Config Methods Type
    var method: String {
        return "GET"
    }
    
    // Config Headers Params
    var headers: Dictionary<String, String> {
        switch self {
        default:
            return [:]
        }
    }
    
    // Config Body Parameters
    var parameters: Dictionary<String, Any> {
        return [:]
    }
}

/** Setup Operation */
struct WebOperation: Requestable {
    
    var router: TestRouter!
    
    var url: NSURL {
        return router.url
    }
    
    var method: String {
        return router.method
    }
    
    var parameters: Dictionary<String, Any> {
        return router.parameters
    }
    
    var headers: Dictionary<String, String> {
        return router.headers
    }
}

/** Client to use Network Operation */
struct ClientClass {
    func fetchData() {
        var networkOperation = WebOperation()
        networkOperation.router = TestRouter.getTodos
        networkOperation.send { result, statusCode in
            // TODO: Handle valid response
        } failure: { error in
            // TODO: Handle Error response
        }
    }
}

/** Call fetchData Method from instance class */
// ClientClass().fetchData()
